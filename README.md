# Qtile

Qtile is written and configured entirely in Python, which means you can leverage the full power and flexibility of the language to make it fit your needs. Learn more about qtile [here](http://www.qtile.org/)

Qtile ducumentations [here](http://docs.qtile.org/en/latest/)

![qtile - darknesscode](/config-files/qtile-a.png)

# What this does

Install qtile for **Arch Linux** and **Debian**

* Debian based-systems
	* Install dependencies
	* Install from source
	* Install from pip (stable)
* Arch based-systems
	* Qtile is in the repositories.
* Config files
	* Copy some config files for this build

![qtile - darknesscode](/config-files/qtile-b.png)

## Software

Here are the software that always install in my systems:

* Ranger file manager
* Nitrogen
* xfce4-power-manager
* xautolock (blurlock)
* urxvt (i like it)
* dnust

# Helpfull Keybindings for this buil

The MODKEY = MOD4 (windows key)

| Keys            | Action                |
| :----------     | :-------------------  |
| MOD+Enter       | Open Terminal (urxvt) |
| MOD+Shift+Enter | Open Terminal (st)    |
| MOD+q           | Close Window          |
| MOD+Ctrl+r      | Restart Qtile         |
| MOD+r           | Ranger Filemanager    |
| MOD+v           | Open Vim              |
| MOD+0           | System Account        |
| MOD+F11         | Networkmanager dmenu  |
| MOD+F12         | Block Screen          |

For more keybindings check the /.config/qtile/confy.py file. And change your terminal before installing.

### Note

Added [NetworkManager-dmenu](https://github.com/firecat53/networkmanager-dmenu) From Firecat53 repo.
